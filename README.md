This project is to control AlphaBot with several options.
AlphaBot is controled by Raspberry Pi3.
The implemented options is following:

	1.Infrared Line Tracking

	2.Infrared Obstacle Avoidance

	3.Infrared Remote Control

	4.Ultrasonic Obstacle Avoiddance
	
This is AlpahaBot's mainboard.

![alt text](AlphaBot.png)

![alt text](mainboard_diagram.png)
